#coding=utf8

########################################################################
###                                                                  ###
### Created by Martin Genet, 2012-2019                               ###
###                                                                  ###
### University of California at San Francisco (UCSF), USA            ###
### Swiss Federal Institute of Technology (ETH), Zurich, Switzerland ###
### École Polytechnique, Palaiseau, France                           ###
###                                                                  ###
########################################################################

from builtins import range

import os
import vtk

import myPythonLibrary as mypy
import myVTKPythonLibrary as myvtk

########################################################################

def readXDMF(
        filename,
        n_timestep,
        verbose=0):

    mypy.my_print(verbose, "*** readXDMF: "+filename+" ***")

    assert (os.path.isfile(filename)), "Wrong filename (\""+filename+"\"). Aborting."

    if   (filename.endswith("xdmf")):
        xdmf_reader = vtk.vtkXdmfReader()
    else:
        assert 0, "File must be .xdmf. Aborting."

    xdmf_reader.SetFileName(filename)
    xdmf_reader.Update()
    xdmf_reader.UpdateInformation()

    exe = xdmf_reader.GetExecutive()
    outInfo = exe.GetOutputInformation(0)
    timeStepsKey = vtk.vtkStreamingDemandDrivenPipeline.TIME_STEPS()
    nTimeSteps = outInfo.Length(timeStepsKey)
    mypy.my_print(verbose-1, "n_timesteps = "+str(nTimeSteps))
    assert n_timestep <= nTimeSteps - 1

    pd2cd = vtk.vtkPointDataToCellData()
    pd2cd.SetInputConnection(xdmf_reader.GetOutputPort())
    pd2cd.PassPointDataOn()

    timeValue = outInfo.Get(timeStepsKey, n_timestep)
    mypy.my_print(verbose-1, "Time step = "+str(n_timestep)+"Time value = "+str(timeValue))
    exe.SetUpdateTimeStep(0, timeValue)
    xdmf_reader.Modified()

    pd2cd.Update()
    data = pd2cd.GetOutput()
    xdmf = xdmf_reader.GetOutputDataObject(0)

    mypy.my_print(verbose-1, "n_points = "+str(xdmf.GetNumberOfPoints()))
    mypy.my_print(verbose-1, "n_cells = "+str(xdmf.GetNumberOfCells()))

    return xdmf
