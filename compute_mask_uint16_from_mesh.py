import vtk
import myVTKPythonLibrary as myvtk

def compute_mask_uint16_from_mesh(image,
                                  mesh,
                                  out_value,
                                  in_value=1,
                                  binary_mask=0,
                                  warp_mesh=0,
                                  output_data_type='uint16'
                                  ):

    input_data_type = image.GetPointData().GetScalars().GetDataType()
    print 'input_data_type = ', input_data_type

    if output_data_type == 'uint16':
        assert out_value <= 65535
        cast = vtk.vtkImageCast()
        cast.SetInputData(image)
        cast.SetOutputScalarTypeToUnsignedShort()
        cast.Update()
        image_for_mask = cast.GetOutput()
    elif output_data_type == 'uint8':
        assert out_value <= 255
        image_for_mask = image

    mask = myvtk.computeMaskFromMesh(image_for_mask,mesh,warp_mesh=warp_mesh,binary_mask=binary_mask,out_value=out_value,in_value=in_value)
    print 'output_data_type = ', mask.GetPointData().GetScalars().GetDataType()
    # assert mask.GetPointData().GetScalars().GetDataType() == 5

    return mask
