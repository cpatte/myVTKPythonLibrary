#coding=utf8

########################################################################
###                                                                  ###
### Created by Martin Genet, 2012-2020                               ###
###                                                                  ###
### University of California at San Francisco (UCSF), USA            ###
### Swiss Federal Institute of Technology (ETH), Zurich, Switzerland ###
### École Polytechnique, Palaiseau, France                           ###
###                                                                  ###
###                                                                  ###
### And Cécile Patte, 2019                                           ###
###                                                                  ###
### INRIA, Palaiseau, France                                         ###
###                                                                  ###
########################################################################

from builtins import range

import vtk

import myPythonLibrary    as mypy
import myVTKPythonLibrary as myvtk

########################################################################

def computeUInt16MaskFromMesh(
        image,
        mesh,
        out_value,
        in_value=1,
        binary_mask=0,
        warp_mesh=0,
        output_data_type='uint16',
        verbose=0):

    mypy.my_print(verbose, "*** computeUInt16MaskFromMesh ***")

    input_data_type = image.GetPointData().GetScalars().GetDataType()
    print 'input_data_type = ', input_data_type

    if output_data_type == 'uint16':
        assert (out_value <= 65535)
        cast = vtk.vtkImageCast()
        cast.SetInputData(image)
        cast.SetOutputScalarTypeToUnsignedShort()
        cast.Update()
        image_for_mask = cast.GetOutput()
    elif output_data_type == 'uint8':
        assert out_value <= 255
        image_for_mask = image

    mask = myvtk.computeMaskFromMesh(
        image=image_for_mask,
        mesh=mesh,
        warp_mesh=warp_mesh,
        binary_mask=binary_mask,
        out_value=out_value,
        in_value=in_value)
        
    print 'output_data_type = ', mask.GetPointData().GetScalars().GetDataType()
    # assert (mask.GetPointData().GetScalars().GetDataType() == 5)

    return mask
